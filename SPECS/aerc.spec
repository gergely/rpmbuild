Name:           aerc
Version:        0.5.2
Release:        1%{?dist}
Summary:        A pretty good email client

License:        BSD
URL:            https://aerc-mail.org/
Source0:        https://git.sr.ht/~sircmpwn/aerc/archive/fb67d1f5a3f6fe875d74581aff59c79817a7b9f4.tar.gz

BuildRequires:  scdoc notmuch-devel
Requires:       notmuch

%description
aerc is an email client that runs in your terminal. It's highly efficient
and extensible, perfect for the discerning hacker.

Some of its more interesting features include:

* Editing emails in an embedded terminal tmux-style, allowing you to check
  on incoming emails and reference other threads while you compose your
  replies
* Render HTML emails with an interactive terminal web browser, highlight
  patches with diffs, and browse with an embedded less session
* Vim-style keybindings and ex-command system, allowing for powerful
  automation at a single keystroke
* First-class support for working with git & email
* Open a new tab with a terminal emulator and a shell running for easy
  access to nearby git repos for parallel work
* Support for multiple accounts, with support for IMAP, Maildir, SMTP, and
  sendmail transfer protocols
* Asynchronous IMAP support ensures the UI never gets locked up by a flaky
  network, as mutt often does
* Efficient network usage - aerc only downloads the information which is
  necessary to present the UI, making for a snappy and bandwidth-efficient
  experience

%prep
%autosetup -n aerc-fb67d1f5a3f6fe875d74581aff59c79817a7b9f4


%build
GOFLAGS=-tags=notmuch make PREFIX=/usr


%install
rm -rf $RPM_BUILD_ROOT
%make_install PREFIX=/usr


%files
%{_bindir}/aerc
%{_mandir}/man1/aerc-search.1.gz
%{_mandir}/man1/aerc.1.gz
%{_mandir}/man5/aerc-config.5.gz
%{_mandir}/man5/aerc-imap.5.gz
%{_mandir}/man5/aerc-maildir.5.gz
%{_mandir}/man5/aerc-notmuch.5.gz
%{_mandir}/man5/aerc-sendmail.5.gz
%{_mandir}/man5/aerc-smtp.5.gz
%{_mandir}/man7/aerc-stylesets.7.gz
%{_mandir}/man7/aerc-templates.7.gz
%{_mandir}/man7/aerc-tutorial.7.gz
%{_datadir}/%{name}/accounts.conf
%{_datadir}/%{name}/aerc.conf
%{_datadir}/%{name}/binds.conf
%{_datadir}/%{name}/filters/hldiff
%{_datadir}/%{name}/filters/html
%{_datadir}/%{name}/filters/plaintext
%{_datadir}/%{name}/stylesets/default
%{_datadir}/%{name}/templates/forward_as_body
%{_datadir}/%{name}/templates/quoted_reply
%license LICENSE



%changelog
* Mon Apr 05 2021 Gergely Polonkai <gergely@polonkai.eu>
- Initial release for version 0.5.2
