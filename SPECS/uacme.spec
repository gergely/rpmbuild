Name:           uacme
Version:        1.7
Release:        1%{?dist}
Summary:        ACMEv2 client written in plain C with minimal dependencies

License:        GPLv3
URL:            https://github.com/ndilieto/uacme
Source0:        https://github.com/ndilieto/%{name}/archive/refs/tags/upstream/%{version}.tar.gz

BuildRequires: gnutls-devel gcc asciidoc libcurl-devel
Requires:       gnutls libcurl

%description


%prep
%autosetup -n uacme-upstream-%{version}


%build
%configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%{_bindir}/uacme
%{_bindir}/ualpn
%{_mandir}/man1/uacme.1.gz
%{_mandir}/man1/ualpn.1.gz
%{_datadir}/%{name}/nsupdate.sh
%{_datadir}/%{name}/uacme.sh
%{_datadir}/%{name}/ualpn.sh
%license COPYING
%doc docs/uacme.html docs/ualpn.html



%changelog
* Mon Mar 29 2021 Gergely Polonkai <gergely@polonkai.eu>
- First RPMified release
